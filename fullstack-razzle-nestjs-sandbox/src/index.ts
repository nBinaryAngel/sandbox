const bootstraper = require('./server').default;
const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000;

async function init() {
  let app = await bootstraper;
  if (module.hot) {
    module.hot.accept('./server', async () => {
      console.log('🔁  HMR Reloading `./server`...');
      try {
        app.close();
        app = await require('./server').default;
        setTimeout(() => {
          app.listen(port);
        }, 1000);
      } catch (error) {
        console.error(error);
      }
    });
    console.info('✅  Server-side HMR Enabled!');
  }

  app.listen(port);
}
init();
export { };
