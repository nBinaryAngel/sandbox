import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CatsController } from './Cats_Controller';


@Module({
  controllers: [CatsController]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) { }
}
