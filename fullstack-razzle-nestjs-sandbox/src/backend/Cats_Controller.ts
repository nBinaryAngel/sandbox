import { Request, Controller, Get } from '@nestjs/common';

@Controller('cats2')
export class CatsController {
  @Get('special')
  public special(@Request() req): string {
    return 'This action returns all cats';
  }
}
