import { NestFactory } from '@nestjs/core';
import { AppModule } from './backend/App_Module';
import { FrontendNotFoundExceptionFilter } from './backend/NotFound_Filter';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .build();

  const app = await NestFactory.create(AppModule);

  app.useGlobalFilters(new FrontendNotFoundExceptionFilter());
  app.setGlobalPrefix('api');
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  return app;
}

export default bootstrap();