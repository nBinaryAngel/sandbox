# Razzle NextJS Sandbox

## Reproduction steps for basic setup
Razzle with typescript template used.
Custom babel alongside with plugins added for decorators support.
Existing react files moved to ui folder.
Backend folder created with basic setup. NotFound Filter used to return browser app.
index.ts adapted to use bootstrap function propelly.
tsconfig with experimentalDecorators, emitDecoratorMetadata
Swagger added.